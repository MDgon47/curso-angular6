import {v4 as uuid} from 'uuid';


export class DestinoViaje{
    private selected: boolean = false;
    public servicios: String[];
    id = uuid();
    constructor(public nombre:string, public u:string){ // u = ulr
        //this.imagenUrl = u;
        this.servicios = ['pileta', 'desayuno'];
    }
    isSelected(): boolean{
        return this.selected;
    }

    setSelected(s: boolean) {
        this.selected = s;
    }
}

