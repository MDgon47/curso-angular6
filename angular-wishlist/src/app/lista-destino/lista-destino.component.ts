import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model'; 
import { DestinosApiClient } from './../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css']
})
export class ListaDestinoComponent implements OnInit {

  @Output() onItemAdded!: EventEmitter<DestinoViaje>;
  updates: string[];

  
  constructor(public destinosApiClient:DestinosApiClient) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.destinosApiClient.suscribeOnChange((d: DestinoViaje) =>{
      if(d != null){
        this.updates.push('Se ah elegido a ' + d.nombre)
      }
    });
  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje) { 
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }



  elegido(d:DestinoViaje){
    this.destinosApiClient.elegir(d);
  }

}
