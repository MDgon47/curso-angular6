import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FomrDestinoViajeComponent } from './fomr-destino-viaje.component';

describe('FomrDestinoViajeComponent', () => {
  let component: FomrDestinoViajeComponent;
  let fixture: ComponentFixture<FomrDestinoViajeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FomrDestinoViajeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FomrDestinoViajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
