import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ReactiveFormsModule, ValidatorFn, Validators } from '@angular/forms';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-fomr-destino-viaje',
  templateUrl: './fomr-destino-viaje.component.html',
  styleUrls: ['./fomr-destino-viaje.component.css']
})
export class FomrDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg!: FormGroup;
  minLongitud = 3;

  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre:['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      url:['']
    });
   }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string):boolean{
    let d= new DestinoViaje(nombre, url)
    this.onItemAdded.emit(d)
    return false;
  }

  /* Validación personalizada */
  nombreValidator(control: FormControl): {[s: string]: boolean}{
    const l = control.value.toString().trim().length;
    if(l>0 && l <5){
      return {invalidNombre: true};
    }
   return null as any;
  }

  /* Validación personalizada parametrizable */
  nombreValidatorParametrizable(minLong: any): ValidatorFn {
    return (control: AbstractControl): { [s: string]: boolean } | null => {
        let l = control.value.toString().trim().length;

	      if (l > 0 && l < minLong) {
		    return {minLongNombre: true};
	      }
    return null;       
    };
}
  
}

